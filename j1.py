#!/usr/bin/env python3
# -*- coding:utf -8 -*-

frase = str(input("Ingrese una frase: "))

# crear una lista con las palabras de la frase pedida a partir
# de un limitador " "
palabritas = frase.split(" ")
# invertir palabras
palabritas_tras = palabritas[::-1]
print(palabritas)
print(palabritas_tras)
