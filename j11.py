#!/usr/bin/env python3
# -*- coding:utf-8 -*-


# funcion que ordena de menos a mayor la lista
def ordenamientoBurbuja(entrada):
    for numPasada in range(len(entrada)-1, 0, -1):
        for i in range(numPasada):
            if entrada[i] > entrada[i+1]:
                temp = entrada[i]
                entrada[i] = entrada[i+1]
                entrada[i+1] = temp


# lista
entrada = [5, 7, 2, 1, 3]
# llamar funcion de metodo burbuja
ordenamientoBurbuja(entrada)
print(entrada)

# ordenamiento burbuja extraido desde
# https://runestone.academy
# /runestone/static/pythoned/SortSearch/ElOrdenamientoBurbuja.html

