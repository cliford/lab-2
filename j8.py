#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

# funcion que crea la lista random y las imprime
def listas(largo):
    i = []
    j = []
    matriz = []
    # agrea 0 según el largo a l amtriz
    for i in range(largo):
        matriz.append([0]*largo)
    # le da valores aleatorios
    for i in range(largo):
        for j in range(largo):
            matriz[i][j] = random.randint(0, 10)
    imprimir_listas(largo, matriz)


# imprimir las listas deseadas
def imprimir_listas(largo, matriz):
    print("MATRIZ ORIGINAL")
    for i in range(largo):
        for j in range(largo):
            print(matriz[i][j], end=" ")
        print()
    imprimir_listas_invertidas(largo, matriz)


# imprmir lista invertida
def imprimir_listas_invertidas(largo, matriz):
    k = 0

    print("MATRIZ INVERTIDA")
    for i in range(largo-1, k-1, -1):
        for j in range(largo):
            print(matriz[i][j], end=" ")
        print()


largo = int(input("Ingrese el largo de la matriz:"))
listas(largo)

