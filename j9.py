#!/usr/bin/env python3
# -*- coding:utf-8 -*-

largo = input("ingrese un tamaño")
matriz = []


# rellenar la matriz segun la diagonal
def rellenar(matriz, largo):
    largo = int(largo)
    for i in range(largo):
        matriz.append([])
        for j in range(largo):
            # cuando los valore de i y j sean igual agregara un "1"
            if i == j:
                matriz[i].append("1")
            else:
                matriz[i].append("0")


# imprimir la matriz
def imprimir(matriz, largo):
    for i in range(largo):
        for j in range(largo):
            print(matriz[i][j], end="")
        print()


# llamar las funciones
rellenar(matriz)
imprimir(matriz)
