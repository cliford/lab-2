#!/usr/bin/env python3
# -*- coding:utf-8 -*-

string = ("¡XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt")

# limpia las "X" de la lista mediante .split con limite "X"
limpio = string.split("X")
for i in range(len(limpio)):
    print(limpio[i], end="")
# Dar vuelta la lista limpia de X
mensaje = limpio[::-1]
print("\n")
# imprimir mensaje limpio e invertido
for i in range(len(mensaje)):
    print(mensaje[i], end="")

