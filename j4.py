#!/usr/bin/env python3
# -*- coding:utf -8 -*-

numeros = [5, 2, 4, 2, 5, 7, 1, 34, 63, 1235]
# crear lista con numeros ordenados de menor a mayor mediante
# sorted
menores = sorted(numeros)
# crear lista de mayor a menos invirtiendo la de menores
mayores = menores[::-1]

multiplos = []

# k es parametro modificable segun sea la multiplicidad
k = 2
for i in range(len(numeros)):
    if numeros[i] % k == 0:
        multiplos.append(numeros[i])


print(multiplos)
print(numeros)
print(menores)
print(mayores)
