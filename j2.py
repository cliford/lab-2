#!/usr/bin/env python3
# -*- coding:utf -8 -*-
lista = []
largo = int(input("ingrese tamaño:"))
# imprimir como una lista segun las posiciones
for i in range(largo):
    # cada lista sigue la logica de
    # un lado crece "*" ambos costados
    # y en medio disminuye " "
    lista.append("*" * (i + 1) + " " * 2 * (largo - (i + 1)) + "*" * (i + 1))

for i in range(largo):
    print(lista[i])

