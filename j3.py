#!/usr/bin/env python3
# -*- coding:utf-8 -*-
largo = int(input("ingrese la base del triangulo"))
largo = largo * 2

# Funcion que imprime las listas deseadas
def imprimir(triangulo, largo):
    for i in range(largo):
        for j in range(largo):
            print(triangulo[i][j], end="")
        print()

# Formacion de triangulo numero uno
def prim_triangulo(triangulo, largo):
    for i in range(largo):
            triangulo.append([])
            # delimitacion de los lados hasta que se forme el triangulo
            for j in range(largo):
                if i + j == largo - 1:
                    # formacion de un lado del triangulo
                    if i >= (largo / 2):
                        triangulo[i].append("/")
                    else:
                        triangulo[i].append(" ")
                if i == j:
                    # formacion del otro lado del triangulo
                    if i >= (largo / 2):
                        triangulo[i].append("\\")
                    else:
                        triangulo[i].append(" ")
                if i == largo - 1:
                    # formacion de la base
                    for j in range(largo):
                        triangulo[i].append("-")
                else:
                    triangulo[i].append(" ")
    triangulo[largo-1][0] = " "
    return triangulo


# Formacion de 2do triangulo
def sgdo_triangulo(triangulo, largo):
    for i in range(largo):
        triangulo.append([])
        for j in range(largo):
            # Formacion de diagonales con limite
            # la formacion de triangulo
            if i + j == largo - 1:
                # primer lador
                if i <= ((largo / 2) - 1):
                    triangulo[i].append("/")
                else:
                    triangulo[i].append(" ")
            # segundo lado
            if i == j:
                if i <= ((largo / 2) - 1):
                    triangulo[i].append("\\")
                else:
                    triangulo[i].append(" ")
            # rellenar la base del triangulo
            if i == 0:
                for j in range(largo):
                    triangulo[i].append("-")
            else:
                triangulo[i].append(" ")
    triangulo[0][0] = " "
    return triangulo

# Triangulos numero 3
def terc_triangulo(triangulo, largo):
    for i in range(largo):
        triangulo.append([])
        for j in range(largo):
            # Ingreso de lineas laterales
            if j + largo == largo:
                triangulo[i].append("|")
            if j == largo-3:
                triangulo[i].append("|")
            # Formación de diagonales
            if i == j:
                triangulo[i].append("\\")
            if j + i == largo-2:
                triangulo[i].append("/")
            # Espacios vacios
            else:
                triangulo[i].append(" ")
    return triangulo

# Creacion de listas que guardarán los triangulos
triangulo_1 = []
triangulo_2 = []
triangulo_3 = []

# Llamar funcion que forma triangulos
triangulo_1 = prim_triangulo(triangulo_1, largo)
triangulo_2 = sgdo_triangulo(triangulo_2, largo)
triangulo_3 = terc_triangulo(triangulo_3, largo)

# Imprimir triangulos
imprimir(triangulo_1, largo)
imprimir(triangulo_2, largo)
imprimir(triangulo_3, largo)
