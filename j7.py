#!/usr/bin/env python3
# -*- coding:utf -8 -*-

# comprobar palabras y agregarlas a lsita diferencias
# retornando la lista
def diferencias(inicial, comprobada):
    dif = []
    for i in inicial:
        for j in comprobada:
            if i in dif:
                continue
            if i in comprobada:
                continue
            if i not in j:
                dif.append(i)
    return dif

# ingeso de frases
frase_1 = str(input("Ingrese una frase: "))
frase_2 = str(input("Ingrese una frase: "))
# restringuir la frase a crear listas con solo las palabras
palabra_1 = frase_1.split(" ")
palabra_2 = frase_2.split(" ")

juntas = palabra_1 + palabra_2
dif_1 = diferencias(palabra_1, palabra_2)
dif_2 = diferencias(palabra_2, palabra_1)

print(juntas)
print(dif_1)
print(dif_2)
